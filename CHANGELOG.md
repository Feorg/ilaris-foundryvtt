# Changelog

## v0.0.10

## v0.0.9

## v0.0.8

## v0.0.7

## v0.0.6

## v0.0.5

## v0.0.4

## v0.0.3

## v0.0.2

-  Bei der Auswahl von voller Defensive wurde im Chat volle Offensive angezeigt
-  Bei der Nutzung des Würfelmenüs im Kampf wurde bei einer Verteidigung der AT Wert benutzt
-  Schnellschuss wurde nicht berechnet
-  Icons hinzugefügt

## v0.0.1
